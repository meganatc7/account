package com.demo.microservices.account.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Bank {
	private String bankNm;
	private String topic;
}
